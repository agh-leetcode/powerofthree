class Solution {
    /*
    public boolean isPowerOfThree(int n) {
        if(n > 1){
            while(n %3 == 0){
                n /= 3;
            }
        }
        return n==1;
    }
    */
    
    
    /*
    public boolean isPowerOfThree(int n) {
        if(n==0) 
            return false;
        if(n==1) 
            return true;
        if(n>1) 
            return n%3==0 && isPowerOfThree(n/3);
        else
            return false;
    }
    */
    
    
    //best solution
    public boolean isPowerOfThree(int n) {
    // 1162261467 = 3^19
    return ( n>0 &&  1162261467%n==0);
    }
}